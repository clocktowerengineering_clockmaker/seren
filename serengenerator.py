# all values taken from 6100240000 HR Series Operating Manual V2.09.

import serial
import time
from datetime import datetime

class SerenRGenerator():

    #####TODO: remove debug
    debug = True
    #####
    defaultBaud = 19200
    defaultTimeout = 10
    defaultPortID = '/dev/tty_RFGen'
    low_range_enabled = False
    terminator = '\r'
    max_power = 5000
    max_voltage = 1000

    def __init__(self):

        print('Initializing Seren RF Generator.')
        pass

    def BoundInput(self,value,low_value=0, high_value=0):

        if value>high_value:
            return high_value
        
        elif value<low_value:
            return low_value

        else:
            return value

    def IntegerCasting(self,value,length):

        # casts to int and truncates length of integer.
        # example - a 3 digit allowed integer must be between 999 and 0.
        # thus, 0<= value <10^3.
        # generalized 0=< value < 10^n for length n.

        intermediate = int(value)

        if intermediate <0:
            
            return 0


        elif intermediate >= 10**(length):

            return int((10**length) -1)

        else:

            return intermediate

    def BuildStringValue(self,value,lowerbound,upperbound,integerlength, command):

        # handles building the string of a valuecommand.

        # bound value.
        boundvalue = self.BoundInput(value,high_value=upperbound,low_value=lowerbound)
        # cast to int.
        integerboundvalue = self.IntegerCasting(boundvalue,length=integerlength)

        # build string and return
        return str(str(integerboundvalue)+' '+str(command))

    def WriteSerial(self,value):
        
        self.SerenSerial.write((str(value)+str(self.terminator)).encode())
        readback = self.SerenSerial.read_until(self.terminator.encode())
        if self.debug:
            print(str(datetime.now())+ ": " + str(value)+ " > " + str(readback))
        return readback.decode()


    def CheckResponseValid(self,value):

        if len(value)==0:

            # handle no comms here.

            return False

        elif len(value)==1:

            # If response is /r.
            

            return True

        elif value[-2]=='N':

            # handle invalid commands here.
            print('N')
            return False

        elif value[-2]=='P':

            # handle parity error here.
            print('P')
            return False

        else:

            return True

    def DecodeASCIIStatus(self,value):

        statusdict = {'ControlSource':'Serial',
                        'RFRegulationSource':'Internal',
                        'SetpointSource':'Serial',
                        'RFOnStatus':False,
                        'ReflectedPowerLimitActive':False,
                        'MaxPowerAlarm':False,
                        'PACurrentLimitActive':False,
                        'RFOnNoAlarm':False,
                        'DissipationLimitActive':False,
                        'CEXStatus':'Master',
                        'PulseModeActive':False,
                        'WaterLowAlarm':True,
                        'HDWAlarm':True,
                        'ExternalInterlockOpen':True,
                        'TempAlarm':False,
                        'CommunicationsFaultAlarm':True
        }
        
        if int(value[0])==0:
            statusdict['ControlSource'] = 'FrontPanel'
        elif int(value[0])==1:
            statusdict['ControlSource'] = 'Analog'
        else:
            statusdict['ControlSource'] = 'Serial'

        if int(value[1])==3:
            statusdict['RFRegulationSource'] = 'Internal'
        else:
            statusdict['RFRegulationSource'] = 'External'

        if int(value[2])==0:
            statusdict['SetpointSource'] = 'FrontPanel'
        elif int(value[2])==1:
            statusdict['SetpointSource'] = 'Analog'
        else:
            statusdict['SetpointSource'] = 'Serial'

        byte_3 = '{0:08b}'.format(int(value[3]))[::-1]

        if int(byte_3[3])==1:
            statusdict['RFOnStatus']=True
        else:
            statusdict['RFOnStatus']=False

        if int(byte_3[2])==1:
            statusdict['ReflectedPowerLimitActive']=True
        else:
            statusdict['ReflectedPowerLimitActive']=False

        if int(byte_3[1])==1:
            statusdict['MaxPowerAlarm']=True
        else:
            statusdict['MaxPowerAlarm']=False

        if int(byte_3[0])==1:
            statusdict['PACurrentLimitActive']=True
        else:
            statusdict['PACurrentLimitActive']=False

        byte_4 = '{0:08b}'.format(int(value[4]))[::-1]
        
        if int(byte_4[3])==1:
            statusdict['RFOnNoAlarm']=False
        else:
            statusdict['RFOnNoAlarm']=True

        if int(byte_4[2])==1:
            statusdict['DissipationLimitActive']=True
        else:
            statusdict['DissipationLimitActive']=False

        if int(byte_4[1])==1:
            statusdict['CEXStatus']='Follower'
        else:
            statusdict['CEXStatus']='Master'

        if int(byte_4[0])==1:
            statusdict['PulseModeActive']=True
        else:
            statusdict['PulseModeActive']=False

        byte_5 = '{0:08b}'.format(int(value[5]))[::-1]
        
        if int(byte_5[3])==1:
            statusdict['WaterLowAlarm']=True
        else:
            statusdict['WaterLowAlarm']=False

        if int(byte_5[2])==1:
            statusdict['HDWAlarm']=True
        else:
            statusdict['HDWAlarm']=False

        if int(byte_5[1])==1:
            statusdict['ExternalInterlockOpen']=True
        else:
            statusdict['ExternalInterlockOpen']=False

        if int(byte_5[0])==1:
            statusdict['TempAlarm']=True
        else:
            statusdict['TempAlarm']=False

        if int(value[6])==1:
            statusdict['CommunicationsFaultAlarm'] = True
        else:
            statusdict['CommunicationsFaultAlarm'] = False


        return statusdict

    def HandleResponse(self,value,longstatuscheck = False,shortstatuscheck = False, reflectedforwardbackwardcheck = False):

        # handles responses. only works with Echo Off.

        # TODO: handle echo mode more better.

        valid = self.CheckResponseValid(value)

        if valid:

            # handle the empty condition
            if len(value) == 1 and value[0]==self.terminator:

                value = '0'
            # handle the lead \n condition.
            
            elif value[0]==self.terminator:
                value = value[1:]

            elif value[0]=='\n':
                value = value[1:]

            # handle responses
            if longstatuscheck:
                
                print(value)
                if len(value)>3:

                    return value[0:-1]
                else:
                    # response is malformed, throw exception.
                    print(value)
                    print(valid)
                    raise Exception
                

            elif shortstatuscheck:

                if len(value)==8:

                    return value[0:-1]
                else:
                    # response is malformed, throw exception.
                    if self.debug:
                        print(value)
                        print(valid)
                    raise Exception

            elif reflectedforwardbackwardcheck:
                if len(value)==16:

                    return value[0:-1]
                else:
                    # response is malformed, throw exception.
                    if self.debug:
                        print(value)
                        print(valid)
                    raise Exception


            else:
                return value[0:-1]
            
        else:
            if self.debug:
                print(value)
                print(valid)
            else:
                print(value)
                print(valid)
                return value[0:-1]

    
    def HandleResponseWithValue(self,value):

        pass


    def StartSerial(self,baud=defaultBaud,serialPort=defaultPortID,timeout=defaultTimeout,parity=serial.PARITY_NONE):

        self.SerenSerial = serial.Serial()
        self.SerenSerial.timeout=timeout
        self.SerenSerial.port=serialPort
        self.SerenSerial.baudrate=baud
        self.SerenSerial.parity=parity
        self.SerenSerial.open()
        
        #TODO: improve handling on echo/noecho. Presently, set to noecho and burn a few cycles just grinding read_all to flush cache since I can't be assured that the pyserial flush will work.
        self.NoEcho()
        for i in range(10):

            self.SerenSerial.read_all()

        # assert serial controls.

        self.AssertSerial()
        self.AssertCommand2()

        return self.SerenSerial

    def AssertSerial(self):

        response = self.WriteSerial('SERIAL')
        response = self.HandleResponse(response)
        return response

    def AssertCommand2(self):

        response = self.WriteSerial('COMMAND2')
        return response

    def Echo(self):

        response = self.WriteSerial('ECHO')
        response = self.HandleResponse(response)
        return response
        
    def NoEcho(self):

        response = self.WriteSerial('NOECHO')
        response = self.HandleResponse(response)
        return response

    def AssertAnalog(self):

        response = self.WriteSerial('ANALOG')
        response = self.HandleResponse(response)
        return response

    def AssertPanel(self):

        response = self.WriteSerial('PANEL')
        response = self.HandleResponse(response)
        return response

    def SetExciterModeMaster(self):

        response = self.WriteSerial('MST')
        response = self.HandleResponse(response)
        return response

    def SetExciterModeFollower(self):

        response = self.WriteSerial('SLV')
        response = self.HandleResponse(response)
        return response

    def SetForwardPowerLeveling(self):

        response = self.WriteSerial('DL')
        response = self.HandleResponse(response)
        return response

    def SetLoadPowerLeveling(self):

        response = self.WriteSerial('EL')
        response = self.HandleResponse(response)
        return response

    def SetPowerControlMode(self):

        response = self.WriteSerial('IR')
        response = self.HandleResponse(response)
        return response

    def SetVoltageControlMode(self):

        response = self.WriteSerial('DR')
        response = self.HandleResponse(response)
        return response

    def SetPowerSetpoint(self,value):
        
        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=self.max_power,integerlength=5,command='W'))
        response = self.HandleResponse(response)
        return response        

    def DisableRFAndZero(self):

        response = self.WriteSerial('WS')
        response = self.HandleResponse(response)
        return response

    def SetPowerSetpointAndEnableRF(self,value,range=low_range_enabled):
       
        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=self.max_power,integerlength=5,command='WG'))
        response = self.HandleResponse(response)
        return response  
        
    def SetVoltageSetpoint(self,value):

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=self.max_voltage,integerlength=4,command='V'))
        response = self.HandleResponse(response)
        return response

    def EnableRFOutput(self):

        response = self.WriteSerial('G')
        response = self.HandleResponse(response)
        return response

    def DisableRFOutput(self):

        response = self.WriteSerial('S')
        response = self.HandleResponse(response)
        return response

    def EnableRFOutputRamping(self):

        response = self.WriteSerial('EU')
        response = self.HandleResponse(response)
        return response

    def DisableRFOutputRamping(self):

        response = self.WriteSerial('DU')
        response = self.HandleResponse(response)
        return response

    def QueryRFOutputRamping(self):

        response = self.WriteSerial('QRE')
        response = self.HandleResponse(response)
        return response

    def QueryForwardPowerOutput(self):

        response = self.WriteSerial('W?')
        response = self.HandleResponse(response)
        return response

    def QueryReflectedPowerOutput(self):

        response = self.WriteSerial('R?')
        response = self.HandleResponse(response)
        return response

    def QueryDCBiasVoltage(self):

        response = self.WriteSerial('0?')
        response = self.HandleResponse(response)
        return response

    def QueryControlVoltage(self):

        response = self.WriteSerial('V?')
        response = self.HandleResponse(response)
        return response

    def QueryPowerLevelingMode(self):

        response = self.WriteSerial('LVL?')
        response = self.HandleResponse(response)
        return response

    def QueryReflectedPowerForwardPowerStatus(self):

        # This appears to have a double \r \r return instead of the single \r, leading to a misordering.
        # Handled via an additional read.

        response = self.WriteSerial('R?W?R')
        response = self.HandleResponse(response,reflectedforwardbackwardcheck=True)
        
        # additional read here.
        self.SerenSerial.read_all()

        # ok, break down response.
        outputdict = self.DecodeASCIIStatus(response[-7:])
        outputdict['Reflected']= int(response[0:4])
        outputdict['Forward'] = int(response[4:8])

        return outputdict, response

    def QueryFirmwareVersion(self):

        response = self.WriteSerial('VERS')
        response = self.HandleResponse(response)
        return response

    def QueryRFAmplifierDCPowerSupplyVoltage(self):

        response = self.WriteSerial('QDCV')
        response = self.HandleResponse(response)
        return response

    def QueryRFAmplifierPowerTransistorDeviceDissipation(self):
                #TODO: boy, this is fuckin weird. Returns a '\n0\r' instead of '0\r'. Investigate.

        response = self.WriteSerial('QDIS')
        response = self.HandleResponse(response)
        return response

    def QueryTotalRFPowerAmplifierCurrent(self):
        #TODO: boy, this is fuckin weird. Returns a '\n0\r' instead of '0\r'. Investigate.

        response = self.WriteSerial('QPAT')
        response = self.HandleResponse(response)
        return response

    def QueryIndividualRFPowerAmplifierCurrent(self):

        response = self.WriteSerial('QPA')
        response = self.HandleResponse(response)
        return response

    def QueryInternalTemperature(self):

        response = self.WriteSerial('QTMP')
        response = self.HandleResponse(response)
        return response

    def QueryStatusLongForm(self):

        response = self.WriteSerial('Q')
        response = self.HandleResponse(response,longstatuscheck=True)

        # ok, break down response.
        outputdict = self.DecodeASCIIStatus(response[0:7])
        # find next space
        setpointend = (8+response[8:].find(' '))
        outputdict['Setpoint'] = int(response[7:setpointend])
        forwardend = setpointend + (1 + response[(setpointend+1):].find(' '))
        outputdict['Forward']= int(response[setpointend:forwardend])
        reflectedend = forwardend + (1 + response[(forwardend+1):].find(' '))
        outputdict['Reflected'] = int(response[forwardend:reflectedend])
        outputdict['MaximumPower'] = int(response[reflectedend:])

        return outputdict, response

    def QueryStatusShortForm(self):

        response = self.WriteSerial('R')
        response = self.HandleResponse(response,shortstatuscheck=True)
        outputdict = self.DecodeASCIIStatus(response[0:7])
        return outputdict, response

    def QueryMaximumPower(self):

        response = self.WriteSerial('M?')
        response = self.HandleResponse(response)
        return response

    def SelectCurrentControlMode(self):

        response = self.WriteSerial('CURR')
        response = self.HandleResponse(response)
        return response

    def SetCurrentSetpoint(self,value):
        value = value * 100 # value is sent in amps.
        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=self.max_power,integerlength=4,command='AMPS'))
        response = self.HandleResponse(response)
        return response  

    def QueryCurrentOutput(self):

        response = self.WriteSerial('QAMP')
        response = self.HandleResponse(response)
        return response

    def QuerySetpoint(self):

        response = self.WriteSerial('QSET')
        response = self.HandleResponse(response)
        return response

    
    def CopyAllProgrammableParametersToFlash(self):

        response = self.WriteSerial('FLSH')
        response = self.HandleResponse(response)
        return response

class PulsingRGenerator(SerenRGenerator):

    def SetPulseDutyCycle(self,value):

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=self.max_voltage,integerlength=3,command='D'))
        response = self.HandleResponse(response)
        return response

    def SetPulseFrequency(self,value):

        pass

    def SetPulseHighTime(self,value):
        pass

    def SetPulseHighPowerSetpoint(self,value):
        pass

    def SetPulseLowPowerSetpoint(self,value):

        pass

    def EnablePulseMode(self):

        response = self.WriteSerial('+P')
        response = self.HandleResponse(response)
        return response

    def DisablePulseMode(self):

        response = self.WriteSerial('-P')
        response = self.HandleResponse(response)
        return response

class VFTRGenerator(PulsingRGenerator):

    pass

class SerenHRGenerator(PulsingRGenerator):
   
    #adds additional HR series values.

    def SetVoltageSetpointMaximumLimit(self,value):

        pass

    def SetSerialParity(self,value):
        
        #TODO: not available on R series
        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=2,integerlength=1,command='PARITY'))
        response = self.HandleResponse(response)
        return response


    def DisablePanel(self):
        #TODO: not available on R series
        response = self.WriteSerial('PSAN')
        response = self.HandleResponse(response)
        return response

    def EnablePanel(self):
        
        #TODO: not available on R series
        response = self.WriteSerial('PSAY')
        response = self.HandleResponse(response)
        return response

    def QueryPanelStatus(self):

        #TODO: not available on R series
        response = self.WriteSerial('QPSA')
        response = self.HandleResponse(response)
        return response

    def SetOperatingFrequency(self,value):

        #TODO: not available on R series or non variable freq supplies
        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=99999,integerlength=5,command='FQ'))
        response = self.HandleResponse(response)
        return response

    def QueryOperatingFrequency(self):

        #TODO: not available on R series or non variable freq supplies
        response = self.WriteSerial('QFRQ')
        response = self.HandleResponse(response)
        return response

    def SetMaximumOperatingFrequencyLimit(self,value):
        #TODO: not available on R series or non variable freq supplies
        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=99999,integerlength=5,command='MAXFQ'))
        response = self.HandleResponse(response)
        return response
        
    def QueryMaximumOperatingFrequencyLimit(self):

        #TODO: not available on R series or non variable freq supplies
        response = self.WriteSerial('QMXFQ')
        response = self.HandleResponse(response)
        return response

    def SetMinimumOperatingFrequencyLimit(self,value):

        #TODO: not available on R series or non variable freq supplies
        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=99999,integerlength=5,command='MINFQ'))
        response = self.HandleResponse(response)
        return response
        
    def QueryMinimumOperatingFrequencyLimit(self):

        #TODO: not available on R series or non variable freq supplies
        response = self.WriteSerial('QMNFQ')
        response = self.HandleResponse(response)
        return response

    def SetExciterModeAuto(self):
        #TODO: not available on R series
        response = self.WriteSerial('EXA')
        response = self.HandleResponse(response)
        return response

    def SetVoltageFeedbackProbeAttenuationFactor(self,value):

        pass

    def QueryControlMode(self):
        #TODO: not available on R series or non variable freq supplies
        response = self.WriteSerial('QREG')
        response = self.HandleResponse(response)
        return response

    def QueryVoltageFeedbackProbeAttenuationFactor(self):

        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QATT')
        response = self.HandleResponse(response)
        return response
        
    def QueryDefaultPowerSetpoint(self):

        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QDFS')
        response = self.HandleResponse(response)
        return response

    def SetMaximumPowerLimit(self,value):

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=99999,integerlength=5,command='MAXP'))
        response = self.HandleResponse(response)
        return response

    def QueryMaximumPowerLimit(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QMXP')
        response = self.HandleResponse(response)
        return response

    def QueryVoltageSetpointMaximumLimit(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QMXV')
        response = self.HandleResponse(response)
        return response

    def QueryPulseDutyCycle(self):

        #TODO: not available on R series or non variable freq supplies
        
        response = self.WriteSerial('QPD')
        response = self.HandleResponse(response)
        return response

    def QueryPulseFrequency(self):

        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QPR')
        response = self.HandleResponse(response)
        return response

    def SelectInternalPulseGateSource(self):

        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('PGI')
        response = self.HandleResponse(response)
        return response

    def QueryInternalPulseGateSource(self):

        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QPG')
        response = self.HandleResponse(response)
        return response

    def SelectExternalPulseGateSource(self):

        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('PDE')
        response = self.HandleResponse(response)
        return response

    def QueryPulseMode(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QPE')
        response = self.HandleResponse(response)
        return response

    def SetVFTCoarseTripRatio(self,value):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=1,upperbound=100,integerlength=3,command='CR'))
        response = self.HandleResponse(response)
        return response

    def QueryVFTCoarseTripRatio(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QCR')
        response = self.HandleResponse(response)
        return response

    def SetVFTCoarseFrequencyStep(self,value):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=10000,integerlength=5,command='CF'))
        response = self.HandleResponse(response)
        return response

    def QueryVFTCoarseFrequencyStep(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QCF')
        response = self.HandleResponse(response)
        return response

    def SetVFTFineFrequencyStep(self,value):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=10000,integerlength=5,command='FF'))
        response = self.HandleResponse(response)
        return response

    def QueryVFTFineFrequencyStep(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QFF')
        response = self.HandleResponse(response)
        return response

    def SetVFTFineTripLevel(self,value):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=1,upperbound=100,integerlength=3,command='FT'))
        response = self.HandleResponse(response)
        return response

    def QueryVFTFineTripLevel(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QFT')
        response = self.HandleResponse(response)
        return response

    def SetVFTSuperFineTripLevel(self,value):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=65535,integerlength=5,command='ATUNST'))
        response = self.HandleResponse(response)
        return response

    def QuerySuperFineTripThreshold(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QSFT')
        response = self.HandleResponse(response)
        return response

    def SetVFTSuperFineGainFrequency(self,value):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=10000,integerlength=5,command='ATUNSG'))
        response = self.HandleResponse(response)
        return response

    def QueryVFTSuperFineGainFrequency(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QSFG')
        response = self.HandleResponse(response)
        return response

    def SetMaximumVFTFrequency(self,value):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=99999,integerlength=5,command='MAXVF'))
        response = self.HandleResponse(response)
        return response

    def QueryMaximumVFTFrequency(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QMXFA')
        response = self.HandleResponse(response)
        return response

    def SetMinimumVFTFrequency(self,value):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=99999,integerlength=5,command='MINVF'))
        response = self.HandleResponse(response)
        return response

    def QueryMinimumVFTFrequency(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QMNFA')
        response = self.HandleResponse(response)
        return response

    def SetVFTStrikeFrequency(self,value):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=99999,integerlength=5,command='SF'))
        response = self.HandleResponse(response)
        return response

    def QueryVFTStrikeFrequency(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QSTK')
        response = self.HandleResponse(response)
        return response

    def EnableVFTTuning(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('VX')
        response = self.HandleResponse(response)
        return response

    def DisableVFTTuning(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('FX')
        response = self.HandleResponse(response)
        return response

    def QueryVFTTuningMode(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QAFT')
        response = self.HandleResponse(response)
        return response

    def SetRFOutputRampDownTimeInterval(self,value):

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=999,integerlength=3,command='DN'))
        response = self.HandleResponse(response)
        return response

    def QueryRFOutputRampDownTimeInterval(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QRD')
        response = self.HandleResponse(response)
        return response

    def SetRFOutputRampUpTimeInterval(self,value):

        response = self.WriteSerial(self.BuildStringValue(value,lowerbound=0,upperbound=999,integerlength=3,command='UP'))
        response = self.HandleResponse(response)
        return response

    def QueryRFOutputRampUpTimeInterval(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QRU')
        response = self.HandleResponse(response)
        return response

    def QueryLastRFOffReason(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QOFF')
        response = self.HandleResponse(response)
        return response

    def SetCommunicationsProtocol(self,value):
        # not implemented; perform on front panel

        pass

    def QueryCommunicationsProtocol(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QSMD')
        response = self.HandleResponse(response)
        return response

    def SetSerialPortBaudRate(self,value):
        # not implemented; perform on front panel
        pass

    def QuerySerialPortBaudRate(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QBR')
        response = self.HandleResponse(response)
        return response

    def EnableInternalRS485Termination(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('TRMY')
        response = self.HandleResponse(response)
        return response

    def DisableInternalRS485Termination(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('TRMN')
        response = self.HandleResponse(response)
        return response

    def QueryInternalRS485Termination(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QTRM')
        response = self.HandleResponse(response)
        return response

    def SetAddressIDNumber(self,value):
        # not implemented; perform on front panel
        pass

    def QueryAddressIDNumber(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QSAD')
        response = self.HandleResponse(response)
        return response

    def SetDeviceNetDataRate(self,value):
        # not implemented; perform on front panel

        pass

    def QueryDeviceNetDataRate(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QDNDR')
        response = self.HandleResponse(response)
        return response

    def SetDeviceNetDataFormat(self,value):
        # not implemented; perform on front panel

        pass

    def QueryDeviceNetDataFormat(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QDNDF')
        response = self.HandleResponse(response)
        return response

    def SetEthernetIPAddress(self,value):
        # not implemented; perform on front panel

        pass

    def QueryEthernetIPAddress(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QENIP')
        response = self.HandleResponse(response)
        return response

    def SetEthernetNetMask(self,value):
        # not implemented; perform on front panel

        pass

    def QueryEthernetNetMask(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QENNM')
        response = self.HandleResponse(response)
        return response

    def SetEthernetGatewayAddress(self,value):
        # not implemented; perform on front panel

        pass

    def QueryEthernetGatewayAddress(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QENGW')
        response = self.HandleResponse(response)
        return response

    def CloseTCPIPConnection(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('ENBYE')
        response = self.HandleResponse(response)
        return response

    def SetAnalogInterfaceFullScale5V(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('RNG5')
        response = self.HandleResponse(response)
        return response

    def SetAnalogInterfaceFullScale10V(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('RNG10')
        response = self.HandleResponse(response)
        return response

    def QueryAnalogInterfaceFullScaleVoltage(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QRNGC')
        response = self.HandleResponse(response)
        return response

    def SetOutputPowerHighRange(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('HR')
        response = self.HandleResponse(response)
        return response

    def SetOutputPowerLowRange(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('LR')
        response = self.HandleResponse(response)
        return response

    def QueryOutputPowerRange(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QGRNP')
        response = self.HandleResponse(response)
        return response

    def EnableXIMPDetection(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('XIMPY')
        response = self.HandleResponse(response)
        return response

    def DisableXIMPDetection(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('XIMPN')
        response = self.HandleResponse(response)
        return response

    def QueryXIMPDetection(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QXZE')
        response = self.HandleResponse(response)
        return response

    def SetXIMPTriggerValue(self,value):
        # not implemented; perform on front panel

        pass

    def QueryXIMPTriggerValue(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QXZT')
        response = self.HandleResponse(response)
        return response

    def SetXIMPDelayValue(self,value):
        # not implemented; perform on front panel

        pass

    def QueryXIMPDelayValue(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QRFONW')
        response = self.HandleResponse(response)
        return response

    def DisableArcSuppression(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('ARCSN')
        response = self.HandleResponse(response)
        return response

    def EnableArcSuppressionReflectedPowerMode(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('ARCSR')
        response = self.HandleResponse(response)
        return response

    def EnableArcSuppressionDCVoltageMode(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('ARCSD')
        response = self.HandleResponse(response)
        return response

    def QueryArcSuppressionFeatureMode(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QASE')
        response = self.HandleResponse(response)
        return response

    def SetArcSuppressionDCVoltageModeThreshold(self,value):
        #TODO: not available on R series or non variable freq supplies

        pass

    def QueryArcSuppressionDCVoltageModeThreshold(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QASTD')
        response = self.HandleResponse(response)
        return response

    def SetArcSuppressionReflectedPowerModeThreshold(self,value):
        #TODO: not available on R series or non variable freq supplies

        pass

    def QueryArcSuppressionReflectedPowerModeThreshold(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QASTR')
        response = self.HandleResponse(response)
        return response

    def SetArcSuppressionPowerReductionRate(self,value):
        #TODO: not available on R series or non variable freq supplies

        pass

    def QueryArcSuppressionPowerReductionRate(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QASDS')
        response = self.HandleResponse(response)
        return response

    def SetArcSuppressionArcSustainPower(self):
        #TODO: not available on R series or non variable freq supplies

        pass

    def QueryArcSuppressionArcSustainPower(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QASSU')
        response = self.HandleResponse(response)
        return response

    def SetArcSuppressionRFOnDelay(self,value):
        #TODO: not available on R series or non variable freq supplies

        pass

    def QueryArcSuppressionRFOnDelay(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QRFONW')
        response = self.HandleResponse(response)
        return response

    def EnableReflectedPowerAlarm(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('REFAY')
        response = self.HandleResponse(response)
        return response

    def DisableReflectedPowerAlarm(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('REFAN')
        response = self.HandleResponse(response)
        return response

    def QueryReflectedPowerAlarm(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QRPAE')
        response = self.HandleResponse(response)
        return response

    def SetReflectedPowerAlarmThreshold(self,value):
        # not implemented; perform on front panel

        pass

    def QueryReflectedPowerAlarmThreshold(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QRPAT')
        response = self.HandleResponse(response)
        return response

    def DisableExcessiveReflectedPowerShutoff(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('0 XRPL')
        response = self.HandleResponse(response)
        return response

    def SetExcessiveReflectedPowerShutoff(self,value):
        # not implemented; perform on front panels

        pass

    def QueryExcessiveReflectedPowerShutoffStatus(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QXRE')
        response = self.HandleResponse(response)
        return response

    def SetExcessiveReflectedPowerRFOnDelayTime(self,value):
        # not implemented; perform on front panel

        pass

    def QueryExcessiveReflectedPowerRFOnDelayTime(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QRFONW')
        response = self.HandleResponse(response)
        return response

    def SetExcessiveReflectedPowerThresholdTimeDelay(self,value):
        # not implemented; perform on front panel

        pass

    def QueryExcessiveReflectedPowerThresholdTimeDelay(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QXRTD')
        response = self.HandleResponse(response)
        return response

    def SetPowerControlGain(self,value):
        # not implemented; perform on front panel

        pass

    def QueryPowerControlGain(self):
        # not implemented; perform on front panel

        response = self.WriteSerial('QPWG')
        response = self.HandleResponse(response)
        return response

    def SetVoltageControlGain(self,value):
        #TODO: not available on R series or non variable freq supplies

        pass

    def QueryVoltageControlGain(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QVCG')
        response = self.HandleResponse(response)
        return response

    def DisableMatchingNetworkPreset(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('PRESN')
        response = self.HandleResponse(response)
        return response

    def EnableMatchingNetworkPresetNormalMode(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('PRESY')
        response = self.HandleResponse(response)
        return response

    def EnableMatchingNetworkPresetInvertedMode(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('PRESI')
        response = self.HandleResponse(response)
        return response

    def SetMatchingNetworkLoadCapacitorPresetPosition(self,value):
        #TODO: not available on R series or non variable freq supplies

        pass

    def SetMatchingNetworkTuneCapacitorPresetPosition(self,value):
        #TODO: not available on R series or non variable freq supplies

        pass

    def SetCommonExciterOutputPhase(self,value):
        #TODO: not available on R series or non variable freq supplies

        pass

    def QueryCommonExciterOutputPhase(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QCXP')
        response = self.HandleResponse(response)
        return response

    def EnableLinkStatusWatchdogTimer(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('LNKTY')
        response = self.HandleResponse(response)
        return response

    def DisableLinkStatusWatchdogTimer(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('LNKTN')
        response = self.HandleResponse(response)
        return response

    def QueryLinkStatusWatchdogTimer(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QLNKE')
        response = self.HandleResponse(response)
        return response

    def SetLinkStatusTimeoutPeriod(self,value):
        #TODO: not available on R series or non variable freq supplies

        pass

    def QueryLinkStatusTimeoutPeriod(self):
        #TODO: not available on R series or non variable freq supplies

        response = self.WriteSerial('QLNKT')
        response = self.HandleResponse(response)
        return response
    
    def SetDefaultPowerSetpoint(self,value):

        pass
