# all values taken from 610016000 ATS Series Operating Manual V2.01.

import serial
import time
from datetime import datetime

class SerenMatch():

    #####TODO: remove debug
    debug = False
    #####
    defaultBaud = 19200
    defaultTimeout = .2
    defaultPortID = '/dev/tty_Matchbox'
    terminator = '\r'
    

    def __init__(self):

        print(str(datetime.now()) + ': Initializing Seren Match')
        pass

    def BoundInput(self,value,low_value=0, high_value=0):

        if value>high_value:
            return high_value
        
        elif value<low_value:
            return low_value

        else:
            return value

    def IntegerCasting(self,value,length):

        # casts to int and truncates length of integer.
        # example - a 3 digit allowed integer must be between 999 and 0.
        # thus, 0<= value <10^3.
        # generalized 0=< value < 10^n for length n.

        intermediate = int(value)

        if intermediate <0:
            
            return 0


        elif intermediate >= 10**(length):

            return int((10**length) -1)

        else:

            return intermediate

    def BuildStringValue(self,value,lowerbound,upperbound,integerlength, command):

        # handles building the string of a valuecommand.

        # bound value.
        boundvalue = self.BoundInput(value,high_value=upperbound,low_value=lowerbound)
        # cast to int.
        integerboundvalue = self.IntegerCasting(boundvalue,length=integerlength)

        # build string and return
        return str(str(integerboundvalue)+' '+str(command))

    def WriteSerial(self,value):
        
        self.SerenSerial.write((str(value)+str(self.terminator)).encode())
        readback = self.SerenSerial.read_until(self.terminator.encode())
        if self.debug:
            print(str(datetime.now())+ ": " + str(value)+ " > " + str(readback))
        return readback.decode()


    def CheckResponseValid(self,value):

        if len(value)==0:

            # handle no comms here.

            return False

        elif len(value)==1:

            # If response is /r.
            

            return True

        elif value[-2]=='N':

            # handle invalid commands here.
            print('N')
            return False

        elif value[-2]=='P':

            # handle parity error here.
            print('P')
            return False

        else:

            return True


    def StartSerial(self,baud=defaultBaud,serialPort=defaultPortID,timeout=defaultTimeout,parity=serial.PARITY_NONE):

        self.SerenSerial = serial.Serial()
        self.SerenSerial.timeout=timeout
        self.SerenSerial.port=serialPort
        self.SerenSerial.baudrate=baud
        self.SerenSerial.parity=parity
        self.SerenSerial.open()
        
        #TODO: improve handling on echo/noecho. Presently, set to noecho and burn a few cycles just grinding read_all to flush cache since I can't be assured that the pyserial flush will work.
        self.NoEcho()
        for i in range(10):

            self.SerenSerial.read_all()

        # assert serial controls.

        return self.SerenSerial


    def HandleResponse(self,value,longstatuscheck = False,shortstatuscheck = False, reflectedforwardbackwardcheck = False):

        # handles responses. only works with Echo Off.

        # TODO: handle echo mode more better.

        valid = self.CheckResponseValid(value)

        if valid:

            # handle the lead \n condition.
            if value[0]=='\n':
                value = value[1:]

            elif reflectedforwardbackwardcheck:
                if len(value)==16:

                    return value[0:-1]
                else:
                    # response is malformed, throw exception.
                    if self.debug:
                        print(value)
                        print(valid)
                    raise Exception


            else:
                return value[0:-1]
            
        else:
            if self.debug:
                print(value)
                print(valid)
            raise Exception
            #TODO: handle exceptions here  

    

    def Echo(self):

        response = self.WriteSerial('ECHO')
        response = self.HandleResponse(response)
        return response
        
    def NoEcho(self):

        response = self.WriteSerial('NOECHO')
        response = self.HandleResponse(response)
        return response

    def Manual(self):
        # sets caps to manual mode.
        response = self.HandleResponse(self.WriteSerial('MLD'))
        response2 = self.HandleResponse(self.WriteSerial('MTN'))
        return response, response2
    
    def Automatic(self):
        # sets caps to auto mode.
        response = self.HandleResponse(self.WriteSerial('ALD'))
        response2 = self.HandleResponse(self.WriteSerial('ATN'))
        return response, response2

    def SetLoadCapPosition(self,load):
        load = load * 10
        self.Manual()
        response = self.HandleResponse(self.WriteSerial(self.BuildStringValue(load,lowerbound=0,upperbound=1000,integerlength=4,command='MVLD')))

        return response

    def SetTuneCapPosition(self,tune):
        self.Manual()
        tune = tune * 10
        response = self.HandleResponse(self.WriteSerial(self.BuildStringValue(tune,lowerbound=0,upperbound=1000,integerlength=4,command='MVTN')))

        return response

    def GetCapPosition(self):
        capacitorPosition = {}
        capacitorPosition['load'] = float(self.HandleResponse(self.WriteSerial('CPLP')))/10
        capacitorPosition['tune'] = float(self.HandleResponse(self.WriteSerial('CPTP')))/10
        print(capacitorPosition)
        
        return capacitorPosition


    def GetSensorData(self):
        sensorData = {}
        
        sensorData['magnitudeerror'] = float(self.HandleResponse(self.WriteSerial('MAG')))
        sensorData['phaseerror'] = float(self.HandleResponse(self.WriteSerial('PHS')))
        sensorData['powersensor'] = float(self.HandleResponse(self.WriteSerial('QPSNS')))
        sensorData['voltageprobe'] = float(self.HandleResponse(self.WriteSerial('V?')))
        sensorData['dcprobevoltage'] = float(self.HandleResponse(self.WriteSerial('QDP')))
        sensorData['rfprobevoltage'] = float(self.HandleResponse(self.WriteSerial('QRP')))
        sensorData['currentprobecurrent'] = float(self.HandleResponse(self.WriteSerial('QIP')))

        return sensorData

    def GetOptionalSensorData(self):
        
        return self.HandleResponse(self.WriteSerial('QVIP'))


